# Babysitter_Kata
Babysitter code kata for OhioHealth
 
## Background

This kata simulates a babysitter working and getting paid for one night. The rules are pretty straight forward.

## The babysitter:

* starts no earlier than 5:00PM
* leaves no later than 4:00AM
* gets paid $12/hour from start-time to bedtime
* gets paid $8/hour from bedtime to midnight
* gets paid $16/hour from midnight to end of job
* gets paid for full hours (no fractional hours)

## Feature

As a babysitter  
In order to get paid for 1 night of work  
I want to calculate my nightly charge  

## Assumptions

Input times are entered as integers as follows:  

* 5:00PM to Midnight entered as 5-12
* 1:00AM to 4:00AM enterd as 1-4

### Pre-requisites

* node
* npm

### Setup

* Clone this repo (i.e. 'git clone https://tjkorona@bitbucket.org/tjkorona/babysitter_kata.git')
* Run 'npm install' in the root project directory to pull in dependancies
* Run 'npm test' to run tests
