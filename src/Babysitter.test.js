import Babysitter from './Babysitter'

describe('babysitter', () => {

  const babysitter = Babysitter()

  let result, expectedResult

  test('Start time and endtime before bedtime and before midnight for 1 hour', () => {
    result = babysitter.calculateCharge(5, 6, 8)
    expectedResult = 1 * 12

    expect(result).toEqual(expectedResult)
  })

  test('Start time and endtime before bedtime and before midnight for multiple hours', () => {
    result = babysitter.calculateCharge(5, 7, 8)
    expectedResult = 2 * 12

    expect(result).toEqual(expectedResult)
  })

  test('Start time after bedtime and endtime before midnight for 1 hour', () => {
    result = babysitter.calculateCharge(8, 9, 7)
    expectedResult = 1 * 8

    expect(result).toEqual(expectedResult)
  })

  test('Start time midnight or after for multiple hours', () => {
    result = babysitter.calculateCharge(12, 3, 7)
    expectedResult = 3 * 16

    expect(result).toEqual(expectedResult)
  })

  test('Start time before bedtime, endtime after bedtime and after midnight', () => {
    result = babysitter.calculateCharge(5, 4, 7)
    expectedResult = (2 * 12) + (5 * 8) + (4 * 16)
 
    expect(result).toEqual(expectedResult)
  })

  test('Validate sart time is before end time', () => {
    result = babysitter.calculateCharge(8, 8, 7)
    expectedResult = 'Error - start time must be before end time'

    expect(result).toEqual(expectedResult)
  })
})