export default () => {

  function adjustTimeForMidnight(inTime) {
    if (inTime <= 4) { inTime += 12 }

    return inTime
  }

  function startTimeAfterEndTime(startTime, endTime) {
    return startTime >= endTime
  }

  return {

    calculateCharge(startTime, endTime, bedTime) {
      
      const afterMidnightRate = 16
      const beforeBedTimeRate = 12
      const afterBedTimeRate = 8
      
      let charge = 0
      
      let adjustedStartTime = adjustTimeForMidnight(startTime)
      let adjustedEndTime = adjustTimeForMidnight(endTime)
      let adjustedBedTime = adjustTimeForMidnight(bedTime)

      if (startTimeAfterEndTime(adjustedStartTime, adjustedEndTime)) return 'Error - start time must be before end time'

      for (let hour = adjustedStartTime; hour < adjustedEndTime; hour++) {

        if (hour >= 12) {
          charge += afterMidnightRate
        } else if (hour < adjustedBedTime) {
          charge += beforeBedTimeRate
        } else {
          charge += afterBedTimeRate
        }
      }      
      
      return charge    
    }
  }
}